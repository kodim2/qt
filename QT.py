class QWidget:
    # класс реализует небольшую часть из фреймворка Qt
    def __init__(self, width, height):
        # конструктор,
        # принимает координаты размеры виджета width и height
        self.Width = width
        self.Height = height
        self.X = 0
        self.Y = 0

    def get_size(self):
        # возвращающает текущие значения размера виджета в виде кортежа
        return self.Width, self.Height

    def move(self, x, y):
        # перемещает виджет в точку с координатами (х,у)
        self.X = x
        self.Y = y

    def get_current_location(self):
        # возвращает текущие координаты виджета в виде кортежа
        return self.X, self.Y


class QPushButton(QWidget):
    # класс реализует кнопку,
    # родительский класс — QWidget
    def push(self):
        # выводит на консоль "На меня нажали"
        print("На меня нажали")


class QCheckBox(QPushButton):
    # класс реализует кнопку выбора,
    # родительский класс — QPushButton
    def __init__(self, width, height):
        # конструктор,
        # вызывает обращение к базовому классу
        super().__init__(width, height)
        self.IsChecked = False

    def push(self):
        # выводит на консоль текст «Check», если до этого галочка не стояла на нем,
        # и «Uncheck» - иначе, при этом меняется состояние кнопки
        if not self.IsChecked:
            print("Check")
            self.IsChecked = True
        else:
            print("Uncheck")
            self.IsChecked = False
